﻿using System;
using System.Collections.Generic;
using TemplateReading.Models;
using TemplateReading.Repositories;

namespace TemplateReading
{
    internal class Program
    {
        /// <summary>
        /// Main method
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
            //POCO

            ICustomerRepository repository = new CustomerRepository();
            Console.WriteLine("------------------------------------All Customer------------------------------------");
            TestSelectAll(repository);
            Console.WriteLine("------------------------------------Customer created------------------------------------");
            TestInsert(repository);
            Console.WriteLine("------------------------------------Customer by name------------------------------------");
            TestSelectName(repository);
            Console.WriteLine("------------------------------------Five Customer------------------------------------");
            TestSelectFive(repository);
            Console.WriteLine("--------------------------------------UPDATING USER------------------------------------");
            TestUpdate(repository);
            Console.WriteLine("------------------------------------Customer by Id------------------------------------");
            TestSelect(repository);
            Console.WriteLine("--------------------------------------PRINTING DUPLICATES------------------------------------");
            TestDuplicates(repository);
            Console.WriteLine("--------------------------------------PRINTING SPENDERS------------------------------------");
            TestSpenders(repository);
            Console.WriteLine("--------------------------------------PRINTING MOST POPULAR GENRE------------------------------------");
            TestGenres(repository);
            // CRUD
        }

        /// <summary>
        /// Gets all the customers from the database
        /// </summary>
        /// <param name="repository"></param>
        static void TestSelectAll(ICustomerRepository repository)
        {
            PrintCustomers(repository.GetAllCustomers());
        }

        /// <summary>
        /// Gets a list with only five customers
        /// </summary>
        /// <param name="repository"></param>
        static void TestSelectFive(ICustomerRepository repository)
        {
            PrintCustomers(repository.GetFiveCustomers());
        }

        /// <summary>
        /// Gets customer by Id
        /// </summary>
        /// <param name="repository"></param>
        static void TestSelect(ICustomerRepository repository)
        {
            PrintCustomer(repository.GetCustomer(54));
        }

        /// <summary>
        /// Gets customer by FirstName
        /// </summary>
        /// <param name="repository"></param>
        static void TestSelectName(ICustomerRepository repository)
        {
            PrintCustomer(repository.GetCustomerByName("Astrid"));
        }

        /// <summary>
        /// Adds new customer with given values
        /// </summary>
        /// <param name="repository"></param>
        static void TestInsert(ICustomerRepository repository)
        {
            Customer test = new Customer()
            {
                FirstName = "xdd",
                LastName = "rhillsssd",
                Country = "Ssensssx",
                PostalCode = "7861232",
                Phone = "3000",
                Email = "frar@hotmail.com"

            };
            if (repository.AddNewCustomer(test))
            {
                Console.WriteLine("User created!");
            }
            else
            {
                Console.WriteLine("Couldnt create user");
            }


        }

        /// <summary>
        /// Gets the number of customers in each country
        /// </summary>
        /// <param name="repository"></param>
        static void TestDuplicates(ICustomerRepository repository)
        {
            printDuplicates(repository.GetDuplicateRows());
        }

        /// <summary>
        /// Shows how much customers has spent
        /// </summary>
        /// <param name="repository"></param>
        static void TestSpenders(ICustomerRepository repository)
        {
            PrintCustomersSpenders(repository.GetHighestSpenders());
        }

        /// <summary>
        /// Shows the most popular genre
        /// </summary>
        /// <param name="repository"></param>
        static void TestGenres(ICustomerRepository repository)
        {
            PrintCustomerGenres(repository.GetCustomerGenre());
        }

        /// <summary>
        /// Updates a customer with given values
        /// </summary>
        /// <param name="repository"></param>
        static void TestUpdate(ICustomerRepository repository)
        {
            Customer test = new Customer()
            {
                FirstName = "Kalle",
                LastName = "Snygg",
                Country = "Uzbekistan",
                PostalCode = "69420",
                Phone = "1122334455990",
                Email = "jalla@gmail.com"
            };
            if (repository.UpdateCustomer(5, test))
            {
                Console.WriteLine("User have been updated.");
            }
            else
            {
                Console.WriteLine("Something went wrong, try again.");
            }
        }


        static void PrintCustomers(IEnumerable<Customer> customers)
        {
            foreach (Customer customer in customers)
            {
                PrintCustomer(customer);
                
            }
        }

        static void printDuplicates(IEnumerable<CustomerCountry> customers)
        {
            foreach(CustomerCountry customer in customers)
            {
                printDuplicate(customer);
            }
        }
        static void PrintCustomersSpenders(IEnumerable<CustomerSpender> customers)
        {
            foreach (CustomerSpender customer in customers)
            {
                printSpenders(customer);
            }
        }

        static void PrintCustomerGenres(IEnumerable<CustomerGenre> customers)
        {
            foreach (CustomerGenre customer in customers)
            {
                printGenres(customer);
            }
        }

        static void printGenres(CustomerGenre customer)
        {
            Console.WriteLine($" {customer.FirstName}   |  {customer.Genre} |  {customer.Count}");
        }

        static void printSpenders(CustomerSpender customer)
        {
            Console.WriteLine($"{customer.CustomerId} | {customer.FirstName}   | {customer.LastName}   |  {customer.Amount} |  {customer.Total}");
        }



        static void PrintCustomer(Customer customer)
        {
            Console.WriteLine($"  {customer.CustomerId} |  {customer.FirstName} |  {customer.LastName} |   {customer.Country}  |  {customer.PostalCode} |  {customer.Phone} |  {customer.Email}");
        }

        static void printDuplicate(CustomerCountry customer)
        {
            Console.WriteLine($"{customer.Country}   |  {customer.DuplicateRow}");
        }
    }
}
