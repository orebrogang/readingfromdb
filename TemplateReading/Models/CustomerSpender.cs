﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TemplateReading.Models
{
    public class CustomerSpender : Customer
    {
        // Additional properties for handling Spender call
        public int Amount { get; set; }
        public decimal Total { get; set; }

    }
}
