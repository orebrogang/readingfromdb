﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TemplateReading.Models
{
    public class CustomerCountry : Customer
    {
        // Additional property for handling Country call
        public int DuplicateRow { get; set; }
    }
}
