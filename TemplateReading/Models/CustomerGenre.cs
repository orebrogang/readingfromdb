﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TemplateReading.Models
{
    public class CustomerGenre : Customer
    {
        // Additional properties for handling Genre call
        public string Genre { get; set; }
        public int Count { get; set; }
    }
}
