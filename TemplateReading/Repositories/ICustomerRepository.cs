﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TemplateReading.Models;

namespace TemplateReading.Repositories
{
    public interface ICustomerRepository
    {
        public Customer GetCustomer(int customerId);
        public Customer GetCustomerByName(string firstName);
        public List<Customer> GetAllCustomers();
        public List<Customer> GetFiveCustomers();
        public List<CustomerCountry> GetDuplicateRows();
        public List<CustomerSpender> GetHighestSpenders();
        public List<CustomerGenre> GetCustomerGenre();
        public bool AddNewCustomer(Customer customer);
        public bool UpdateCustomer(int customerId, Customer customer);
    }
}
