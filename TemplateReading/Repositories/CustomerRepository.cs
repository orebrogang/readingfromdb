﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TemplateReading.Models;

namespace TemplateReading.Repositories
{
    public class CustomerRepository : ICustomerRepository
    {
        /// <summary>
        /// Gets all the customers from the database
        /// </summary>
        /// <returns>A list with all the customers from the database</returns>
        public List<Customer> GetAllCustomers()
        {
            List<Customer> CustomerList = new List<Customer>();
            string sql = "SELECT CustomerId ,FirstName, LastName, Country, PostalCode, Phone, Email FROM Customer";
            try
            {
                //Connect
                using (SqlConnection connection = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    connection.Open();
                    //Make a command
                    using (SqlCommand cmd = new SqlCommand(sql, connection))
                    {
                        //Reader
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                //Handle result
                                Customer temp = new Customer();
                                temp.CustomerId = reader.GetInt32(0);
                                temp.FirstName = reader.GetString(1);
                                temp.LastName = reader.GetString(2);
                                temp.Country = reader.GetString(3);
                                temp.PostalCode = Methods.CheckIfNull(reader, 4);
                                temp.Phone = Methods.CheckIfNull(reader, 5);
                                temp.Email = Methods.CheckIfNull(reader, 6);
                                CustomerList.Add(temp);
                            }
                        }
                    }

                }
            }
            catch (SqlException)
            {
                Console.WriteLine("it failed hard");
            }
            return CustomerList;
        }

        /// <summary>
        /// Gets only five customers from the database
        /// </summary>
        /// <returns>A list with five customers</returns>
        public List<Customer> GetFiveCustomers()
        {
            List<Customer> CustomerList = new List<Customer>();
            string sql = "SELECT CustomerId ,FirstName, LastName, Country, PostalCode, Phone, Email FROM Customer ORDER BY FirstName OFFSET 0 ROWS FETCH NEXT 5 ROWS ONLY";
            try
            {
                //Connect
                using (SqlConnection connection = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    connection.Open();
                    //Make a command
                    using (SqlCommand cmd = new SqlCommand(sql, connection))
                    {
                        //Reader
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                //Handle result
                                Customer temp = new Customer();
                                temp.CustomerId = reader.GetInt32(0);
                                temp.FirstName = reader.GetString(1);
                                temp.LastName = reader.GetString(2);
                                temp.Country = reader.GetString(3);
                                temp.PostalCode = Methods.CheckIfNull(reader, 4);
                                temp.Phone = Methods.CheckIfNull(reader, 5);
                                temp.Email = Methods.CheckIfNull(reader, 6);
                                CustomerList.Add(temp);
                            }
                        }
                    }

                }
            }
            catch (SqlException)
            {
                Console.WriteLine("it failed hard");
            }
            return CustomerList;
        }

        /// <summary>
        /// Gets a customer by Id from the database
        /// </summary>
        /// <param name="customerId">Id of the customer</param>
        /// <returns>The customer from the database</returns>
        public Customer GetCustomer(int customerId)
        {
            Customer customer = new Customer();
            string sql = "SELECT CustomerId ,FirstName, LastName, Country, PostalCode, Phone, Email FROM Customer" + " WHERE @CustomerId = CustomerId";


            try
            {
                using (SqlConnection connection = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    connection.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, connection))
                    {
                        cmd.Parameters.AddWithValue("CustomerId", customerId);
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                customer.CustomerId = reader.GetInt32(0);
                                customer.FirstName = reader.GetString(1);
                                customer.LastName = reader.GetString(2);
                                customer.Country = reader.GetString(3);
                                customer.PostalCode = Methods.CheckIfNull(reader, 4);
                                customer.Phone = Methods.CheckIfNull(reader, 5);
                                customer.Email = Methods.CheckIfNull(reader, 6);
                            }
                        }
                    }
                }
            }
            catch (SqlException e)
            {

                Console.WriteLine("Went south try again");
            }

            return customer;

        }

        /// <summary>
        /// Gets a customer by FirstName from the database
        /// </summary>
        /// <param name="firstName">Customers first name</param>
        /// <returns>The customer from the database</returns>
        public Customer GetCustomerByName(string firstName)
        {

            Customer customerName = new Customer();
            string sql = "SELECT CustomerId ,FirstName, LastName, Country, PostalCode, Phone, Email FROM Customer" + " WHERE @FirstName = FirstName";


            try
            {
                using (SqlConnection connection = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    connection.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, connection))
                    {
                        cmd.Parameters.AddWithValue("FirstName", firstName);
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                customerName.CustomerId = reader.GetInt32(0);
                                customerName.FirstName = reader.GetString(1);
                                customerName.LastName = reader.GetString(2);
                                customerName.Country = reader.GetString(3);
                                customerName.PostalCode = Methods.CheckIfNull(reader, 4);
                                customerName.Phone = Methods.CheckIfNull(reader, 5);
                                customerName.Email = Methods.CheckIfNull(reader, 6);
                            }
                        }
                    }
                }
            }
            catch (SqlException e)
            {

                Console.WriteLine("Went south try again");
            }

            return customerName;
        }


        /// <summary>
        /// Adds a new customer to the database
        /// </summary>
        /// <param name="customer">???</param>
        /// <returns>Insert command to add the new customer</returns>
        public bool AddNewCustomer(Customer customer)
        {
            bool success = false;
            string sql = "INSERT INTO Customer( FirstName, LastName, Country, PostalCode, Phone, Email) " +
                " VALUES(@FirstName, @LastName, @Country, @PostalCode, @Phone, @Email)";
            try
            {
                using (SqlConnection connection = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    connection.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, connection))
                    {
                        cmd.Parameters.AddWithValue("@FirstName", customer.FirstName);
                        cmd.Parameters.AddWithValue("@LastName", customer.LastName);
                        cmd.Parameters.AddWithValue("@Country", customer.PostalCode);
                        cmd.Parameters.AddWithValue("@PostalCode", customer.PostalCode);
                        cmd.Parameters.AddWithValue("@Phone", customer.Phone);
                        cmd.Parameters.AddWithValue("@Email", customer.Email);
                        success = cmd.ExecuteNonQuery() > 0 ? true : false;
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex);
            }
            return success;
        }

        /// <summary>
        /// Updates a existing customer on the database
        /// </summary>
        /// <param name="customerId">Id of the customer</param>
        /// <param name="customer">???</param>
        /// <returns>Update command to pass the new values for the customer</returns>
        public bool UpdateCustomer(int customerId, Customer customer)
        {
            bool success = false;
            string sql = "UPDATE Customer " +
                " SET FirstName=@FirstName, LastName=@LastName, Country=@Country, PostalCode=@PostalCode, Phone=@Phone, Email=@Email WHERE @CustomerId = CustomerId";
            try
            {
                using (SqlConnection connection = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    connection.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, connection))
                    {
                        cmd.Parameters.AddWithValue("CustomerId", customerId);
                        cmd.Parameters.AddWithValue("@FirstName", customer.FirstName);
                        cmd.Parameters.AddWithValue("@LastName", customer.LastName);
                        cmd.Parameters.AddWithValue("@Country", customer.Country);
                        cmd.Parameters.AddWithValue("@PostalCode", customer.PostalCode);
                        cmd.Parameters.AddWithValue("@Phone", customer.Phone);
                        cmd.Parameters.AddWithValue("@Email", customer.Email);
                        success = cmd.ExecuteNonQuery() > 0 ? true : false;
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex);
            }
            return success;

        }


        /// <summary>
        /// Gets the number of customers in each country
        /// </summary>
        /// <returns>List with the numbers in each country</returns>
        public List<CustomerCountry> GetDuplicateRows()
        {
            List<CustomerCountry> CustomerList = new List<CustomerCountry>();
            string sql = "SELECT Country, COUNT(Country) as 'Duplicate rows' FROM Customer GROUP BY Country HAVING COUNT (Country) > 0 ORDER BY 'Duplicate Rows' DESC";

            try
            {
                using (SqlConnection connection = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    connection.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, connection))
                    {
                        //Reader
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                //Handle result
                                CustomerCountry temp = new CustomerCountry();
                                temp.Country = reader.GetString(0);
                                temp.DuplicateRow = reader.GetInt32(1);
                                CustomerList.Add(temp);
                            }
                        }
                    }
                }

                return CustomerList;
            }
            catch (SqlException ex)
            {

                Console.WriteLine("Something went wrong" + ex);
            }
            return CustomerList;
        }

        /// <summary>
        /// Get the amount customer have spent
        /// </summary>
        /// <returns>
        /// Returns a sorted list that shows highest to lowest spender
        /// </returns>
        public List<CustomerSpender> GetHighestSpenders()
        {
            List<CustomerSpender> CustomerList = new List<CustomerSpender>();
            string sql = "SELECT Firstname, LastName, Invoice.CustomerId,COUNT(*) as amount, SUM(Total) as mysum FROM Invoice INNER JOIN Customer ON Invoice.CustomerId = Customer.CustomerId GROUP BY Customer.FirstName, Customer.LastName, Invoice.CustomerId ORDER BY mysum DESC";

            try
            {
                using (SqlConnection connection = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    connection.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, connection))
                    {
                        //Reader
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                //Handle result
                                CustomerSpender temp = new CustomerSpender();
                                temp.FirstName = reader.GetString(0);
                                temp.LastName = reader.GetString(1);
                                temp.CustomerId = reader.GetInt32(2);
                                temp.Amount = reader.GetInt32(3);
                                temp.Total = reader.GetDecimal(4);
                                CustomerList.Add(temp);
                            }
                        }
                    }
                }

                return CustomerList;
            }
            catch (SqlException ex)
            {

                Console.WriteLine("Something went wrong" + ex);
            }
            return CustomerList;
        }

        /// <summary>
        /// Get the most popular genre based on customer song purchases
        /// </summary>
        /// <returns>
        /// Returns a customer with the highest purchases in a single genre
        /// </returns>
        public List<CustomerGenre> GetCustomerGenre()
        {
            List<CustomerGenre> CustomerList = new List<CustomerGenre>();
            string sql = "SELECT TOP 1 WITH TIES FirstName, Genre.Name, Count(*) AS Count FROM Customer INNER JOIN Invoice ON Customer.CustomerId = Invoice.CustomerId INNER JOIN InvoiceLine ON Invoice.InvoiceId = InvoiceLine.InvoiceId INNER JOIN Track ON Track.TrackId = InvoiceLine.TrackId INNER JOIN Genre ON Genre.GenreId = Track.GenreId GROUP BY Customer.FirstName, Genre.Name ORDER BY Count DESC;";

            try
            {
                using (SqlConnection connection = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    connection.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, connection))
                    {
                        //Reader
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                //Handle result
                                CustomerGenre temp = new CustomerGenre();
                                temp.FirstName = reader.GetString(0);
                                temp.Genre = reader.GetString(1);
                                temp.Count = reader.GetInt32(2);
                                CustomerList.Add(temp);
                            }
                        }
                    }
                }

                return CustomerList;
            }
            catch (SqlException ex)
            {

                Console.WriteLine("Something went wrong" + ex);
            }
            return CustomerList;
        }
    }

}
