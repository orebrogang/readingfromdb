﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TemplateReading
{
    public static class Methods
    {
        /// <summary>
        /// Method to read nullable values from the tables
        /// </summary>
        /// <param name="reader"></param>
        /// <param name="columnIndex"></param>
        /// <returns>A empty string</returns>
        public static string CheckIfNull(this SqlDataReader reader, int columnIndex)
        {
            if (!reader.IsDBNull(columnIndex))
            {
                return reader.GetString(columnIndex);
            }
            return string.Empty;
        }
    }
}
