# Reading Data From A SQL Database
### Using Chinnok Database

## Prerequisites

1. Have SQL-Server Management Studio and visual studio 2019-2022 installed


## Installation

1. Download Chinnok SqlServer
2. Open it in SQL-Server Management Studio and run the queries
3. git clone https://gitlab.com/orebrogang/readingfromdb.git
4. Install SQL Client Library in the application
5. Change the DataSource string to your localDB one

## Queries

1. Displays all customers from the database
2. Displays a specific customer by customerId
3. Displays a customer by name
4. Displays a limit amount of customers
5. Adds an new customer to the database
6. Updates an exisiting customer
7. Displays the number of customer in each country, starting with highest and ends with lowest
8. Displays whos the highest spenders of the customers
9. Displays the most popular genre of a given customer

## Contributors

#### Anders Hansen-Haug
#### Alexander Majoros
#### Carl Jägerhill
